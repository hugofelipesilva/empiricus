
=== Tema Empiricus para WordPress ===
Data: 18/09/2016
Desenvolvedor: Hugo Felipe

Tema desenvolvido para o processo seletivo da Empiricus

== Instalação ==

1. No painel administrativo do WordPress vá até Aparência -> Temas e clique no botão 'Adicionar Novo'.
2. Clique no botão 'Fazer upload do tema' e escolha o zip compactado desta pasta.
3. Após o upload e o tema aparecer na lista de temas instalados, clique no botão 'Ativar' que aparecerá logo abaixo da imagem do tema.


== Banco de Dados ==

Terminada a instalação e ativação do tema, faça uma importação do arquivo 'hritsolutions2_empiricus.sql' que se encontra na pasta do tema.
O arquivo supracitado contém das informações contidas nas pages, posts e widgets do projeto.


== Dependências ==

O projeto possui as seguintes dependências:

Javascript:
- jQuery
- jQuery Easing
- jQuery Validate
- Selectivizr
- Chrome Frame
- HtmlShiv

CSS:
- 


===