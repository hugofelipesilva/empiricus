var didScroll       = null;
var lastScrollTop   = 0;
var delta           = 5;
var navbarHeight    = $('header').outerHeight();
var menuStatus      = false;
var site            = {
    init: function(){
        site.generics();
        site.hasScrolled();
        site.scroll();
        site.resize();
        new WOW().init();
    },
    generics: function(){
        setInterval(function() {
            if (didScroll) {
                site.hasScrolled();
                didScroll = false;
            }
        }, 250);

        // Internal links click
        $('a[href^="#"]').on('click', function() {  
            var $link = $(this).attr('href');
            if($link=="#button1"){
                $('html, body').stop().animate({'scrollTop': (($($link).offset().top)-175)}, 1000, 'swing');
            }else{
                if($link=="#topo"){
                    $('html, body').stop().animate({'scrollTop': ($($link).offset().top-110)}, 1000, 'swing');
                }else{
                    $('html, body').stop().animate({'scrollTop': $($link).offset().top }, 1000, 'swing');
                }
            }
            return false;
        });

        // External links click
        $('a[href^="http://"],a[href^="https://"]').on('click', function() {        
            var link = $(this).attr('href');
            window.open(link);
            return false;
        });

        // Menu mobile click
        $('button').on('click', function() {
            $(this).toggleClass('expanded');
            if(menuStatus == false){
                menuStatus = true;
                openMobileMenu();
                
            }else{
                menuStatus = false;
                closeMobileMenu();
            }
        });

        openMobileMenu = function(){
            $('header nav ul').slideDown();
        },
        closeMobileMenu = function(){
            $('header nav ul').slideUp('fast');
        }
    },
    hasScrolled: function(params) {
        var st = $(this).scrollTop();
        if(Math.abs(lastScrollTop - st) <= delta)
            return;

        if (st > lastScrollTop && st > navbarHeight){
            if ($(window).width() < 768) {
                menuStatus = false;
               $('button').removeClass('expanded').next('ul').hide();
            }
            else {
                menuStatus = true;
               $('button').removeClass('expanded').next('ul').show();
            }
            $('header').removeClass('nav-down').addClass('nav-up');
        } else {
            if(st + $(window).height() < $(document).height()) {
                $('header').removeClass('nav-up').addClass('nav-down');
            }
        }
        lastScrollTop = st;
    },
    scroll: function(){
        $(window).scroll(function(event){
            didScroll = true;
        });
    },
    resize: function(){
        $(window).resize(function(event){
            if ($(window).width() < 768) {
                menuStatus = false;
               $('button').removeClass('expanded').next('ul').hide();
            }
            else {
                menuStatus = true;
               $('button').removeClass('expanded').next('ul').show();
            }
        });
    }
}

site.init();