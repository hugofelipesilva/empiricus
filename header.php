<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Empiricus
 * @since Empiricus
 */

?><!doctype html>
<!--[if lt IE 7]> <html lang="pt-br" class="no-js ie6 oldie"> <![endif]-->
<!--[if IE 7]>    <html lang="pt-br" class="no-js ie7 oldie"> <![endif]-->
<!--[if IE 8]>    <html lang="pt-br" class="no-js ie8 oldie"> <![endif]-->
<!--[if gt IE 8]><!-->
<html <?php language_attributes(); ?> class="no-js" lang="pt-br">
<!--<![endif]-->
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
		<?php endif; ?>
		<?php

		$description = get_bloginfo( 'description', 'display' );
		if ( $description || is_customize_preview() ) : ?>
			<meta name="description" content="<?php echo $description; ?>" />
		<?php endif; ?>

		<?php wp_head(); ?>
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/css/common/wow.animate.css">
        <link rel="shortcut icon" type="image/png" href="//content.empiricus.com.br/media/images/favicon.png">
        <link rel="apple-touch-icon" href="//content.empiricus.com.br/media/images/apple-touch-icon.png">

        <meta property="og:image" content="http://www.empiricus.com.br/wp-content/themes/empiricus/assets/images/meta-og-image.jpg">
        <!--[if lt IE 9]>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
        <![endif]-->
    </head>
    <body <?php body_class(); ?>>
        <header class="nav-down">
            <div class="hold">
				<?php if ( get_header_image() ) : ?>
					<?php
						/**
						 * Filter the default twentysixteen custom header sizes attribute.
						 *
						 * @since Empiricus
						 *
						 * @param string $custom_header_sizes sizes attribute
						 * for Custom Header. Default '(max-width: 709px) 85vw,
						 * (max-width: 909px) 81vw, (max-width: 1362px) 88vw, 1200px'.
						 */
						$custom_header_sizes = apply_filters( 'twentysixteen_custom_header_sizes', '(max-width: 709px) 85vw, (max-width: 909px) 81vw, (max-width: 1362px) 88vw, 1200px' );
					?>
					
						<a class="logo fl " href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
							<img src="<?php header_image(); ?>" srcset="<?php echo esc_attr( wp_get_attachment_image_srcset( get_custom_header()->attachment_id ) ); ?>" sizes="<?php echo esc_attr( $custom_header_sizes ); ?>" width="<?php echo esc_attr( get_custom_header()->width ); ?>" height="<?php echo esc_attr( get_custom_header()->height ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
						</a>
					
				<?php endif; // End header image check. ?>
                    
                <?php if ( has_nav_menu( 'primary' ) ) : ?>
					<nav class="main-navigation fr" role="navigation" aria-label="<?php esc_attr_e( 'Footer Primary Menu', 'twentysixteen' ); ?>">
					<button>Menu</button>
						<?php
							wp_nav_menu( array(
								'theme_location' => 'primary',
								'menu_class'     => 'primary-menu no-bullet',
							 ) );
						?>
					</nav><!-- .main-navigation -->
				<?php endif; ?>
            </div>
        </header>
<!--
<div id="page" class="site">
	<div class="site-inner">
		<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'twentysixteen' ); ?></a>
		
		
		<header id="masthead" class="site-header" role="banner">
			<div class="site-header-main">
				<div class="site-branding">
					<?php twentysixteen_the_custom_logo(); ?>

					<?php if ( is_front_page() && is_home() ) : ?>
						<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
					<?php else : ?>
						<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
					<?php endif;

					$description = get_bloginfo( 'description', 'display' );
					if ( $description || is_customize_preview() ) : ?>
						<p class="site-description"><?php echo $description; ?></p>
					<?php endif; ?>
				</div>
				

				
			</div>-->
		</header><!-- .site-header -->
