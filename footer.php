<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Empiricus
 * @since Empiricus
 */
?>

		<?php
			/**
			 * Fires before the twentysixteen footer text for footer customization.
			 *
			 * @since Empiricus
			 */
			do_action( 'twentysixteen_credits' );
		?>
		
		<footer>
            <div class="hold">
            	<?php get_sidebar( 'content-bottom' ); ?>
           	</div>
            <div class="hold">
            	<?php if ( is_active_sidebar( 'sidebar-4' ) ) : ?>
					<div class="widget-area">
						<?php dynamic_sidebar( 'sidebar-4' ); ?>
					</div><!-- .widget-area -->
				<?php endif; ?>
            </div>
    	</footer>

	</div><!-- .site-inner -->
</div><!-- .site -->

		<script src="https://code.jquery.com/jquery-2.2.4.min.js"   integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="   crossorigin="anonymous"></script>
        <script src="<?php bloginfo('template_directory'); ?>/js/jquery/jquery.easing.1.3.js"></script>
        <script src="<?php bloginfo('template_directory'); ?>/js/jquery/jquery.validate.js"></script>
        <script src="<?php bloginfo('template_directory'); ?>/js/wow/wow.min.js"></script>
        <!--[if (gte IE 6)&(lte IE 8)]>
            <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/selectivizr/selectivizr-min.js"></script>
        <![endif]-->
        <!--[if lt IE 7]>
            <script src='//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js'></script>
            <script>
                window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})});
            </script>
        <![endif]-->
        <script src="<?php bloginfo('template_directory'); ?>/js/main.js"></script>

<?php wp_footer(); ?>
</body>
</html>
