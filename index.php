<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Empiricus
 * @since Empiricus
 */

get_header(); ?>
	
	<main id="topo">
	    <section class="first-step" preload="auto">
	        <div class="hold">
	            <div>
	                <h1 class="text-center text-uppercase margin-bottom-0"><strong>Como ganhar dinheiro</strong> diante das variações do Dólar?</h1>
	                <h2 class="h1 text-center text-uppercase margin-top-0">Um guia estratégico para tempos de incertezas.</h2>
	                <p class="text-center">Preencha seu email abaixo que te enviaremos o relatório gratuito</p>
	                <form>
	                    <fieldset>
	                        
	                    </fieldset>
	                </form>
	            </div>
	            <div id="button1" class="arrow arrow-white bottom pa wow bounce animated" data-wow-iteration="10"><a href="#button1" title="Clique para ir para o próximo bloco de conteúdo"><span class="bottom"></span></a></div>
	            <div>
	                <h6 class="h1 text-uppercase">Por que baixar esse relatório?</h6>
	                <p>
	                    A Empiricus é totalmente independente e tem o mais alto grau de compromisso com o leitor.<br />
	                    Em 2013, recomendamos aos nossos leitores que comprassem Dólar, na época cotado a R$1,90. Em 2014, já falávamos que ele chegaria a R$4,00 quando ninguém acreditava ser possível.<br />
	                    Saiba agora como você deve se posicionar em relação à moeda norte-americana nos próximos meses.
	                </p>
	            </div>
	        </div>
	    </section>
	    <section id="step_2" class="second-step">
	        <div class="hold">
	            <p class="text-uppercase">Você saberá ainda.</p>
	            <h1 class="text-uppercase">Quais fatores influenciam o comportamento do <strong>Dólar</strong>?</h1>
	            <p>Muitos. Por isso, designamos nossos analistas a avaliá-los um a um, atribuindo a eles seu devido peso.</p>
	            <p>As decisões do Banco Central americano (Fed) e a quantidade de dinheiro circulando na economia por exemplo, afetam o preço do Dólar frente ao Real.</p>
	            <p>Isso modifica o cenário econômico dos principais países do mundo e tem impacto inclusive no bolso de pessoas comuns.</p>
	            <h1 class="text-uppercase">O que a <strong>alta do Dólar</strong> tem a ver com seu bolso?</h1>
	            <p>O comportamento do Dólar influencia diretamente os preços de bens em váios setores da economia brasileira, como gasolina, arroz, feijão, imóveis, automóveis, aluguel, energia elétrica, os smartphones e outros produtos e serviços do nosso dia-a-dia.</p>
	            <p>Por isso é necessário analisar detalhadamente todas as variáveis domésticas e internacionais que influenciam na taxa de câmbio do Dólar e do Real.</p>
	            <p>“Vou sair de férias e o Dólar está caro, como eu faço?”. Neste relatório explicamos ainda ao que você deve estar atento quando irá viajar e precisa comprar Dólar.</p>
	            <h1 id="vale" class="text-uppercase">Ainda vale a pena <strong>comprar Dólares para investir</strong>, ou já perdi a oportunidade?</h1>
	            <p>Muita coisa pode mudar em poucas semanas quando o assunto é câmbio, ainda mais neste momento nebuloso no qual vivemos. Saiba a opinião de nossos analistas sobre como se posicionar.</p>
	            <p class="text-center"><a class="button red" href="#topo" title="Clique para voltar ao topo da página"><span class="arrow top"></span>Voltar ao Topo</a></p>
	        </div>
	    </section>
	    <section id="empiricus" class="third-step" preload="auto">
	        <div class="hold">
	            <div class="img-box pa fr"><a href="http://empiricus.com.br" title="Clique e saiba mais sobre a Empiricus"><img src="assets/img/logo_institucional_diagnonal_empiricus-research.png" alt="Empiricus Research" /></a></div>
	            <h1>Conheça a Empiricus</h1>
	            <p>Fundada em 2009, a <strong>Empiricus Research</strong> é a primeira consultoria independente de investimentos do Brasil que oferece conteúdo gratuito e educativo para todos os tipos de investidores.</p>
	            <a class="button lg red fl" href="http://www.empiricus.com.br/" title="Saiba mais sobre a Empiricus Research">Saiba mais</a>
	        </div>
	    </section>
	</main>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ) : ?>

			<?php if ( is_home() && ! is_front_page() ) : ?>
				<header>
					<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
				</header>
			<?php endif; ?>

			<?php
			// Start the loop.
			while ( have_posts() ) : the_post();

				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				get_template_part( 'template-parts/content', get_post_format() );

			// End the loop.
			endwhile;

			// Previous/next page navigation.
			the_posts_pagination( array(
				'prev_text'          => __( 'Previous page', 'twentysixteen' ),
				'next_text'          => __( 'Next page', 'twentysixteen' ),
				'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentysixteen' ) . ' </span>',
			) );

		// If no content, include the "No posts found" template.
		else :
			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
